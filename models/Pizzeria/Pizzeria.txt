Title:
Pizzeria

Description:
This collaboration shows the interactions between multiple Customer and a Pizzeria.
Each instantiated Customer read the menu of the pizzeria and choose the desired one.
The the Customer communicate this choice to the Pizzeria that creates a new instance.
Then, the Pizzeria check if the placed order is correct, if so, it prepare the pizza and give it to the Customer.

Versions:
Pizzeria.bpmn - Correct version
