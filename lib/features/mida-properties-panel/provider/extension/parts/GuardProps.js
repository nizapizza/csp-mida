'use strict';

var guardEntryFactory = require('./implementation/Guard'),
    is = require('bpmn-js/lib/util/ModelUtil').is;

module.exports = function(group, element, translate) {

  if (is(element, 'bpmn:Task')) {

    var options = { modelProperty: 'guard' };
    group.entries = group.entries.concat(guardEntryFactory(element, options, translate));

  }

};
