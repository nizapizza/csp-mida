
import {
  is
} from 'bpmn-js/lib/util/ModelUtil';
import eventDefinitionHelper from 'bpmn-js-properties-panel/lib/helper/EventDefinitionHelper';
var getBusinessObject = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;
var midaFactory = require('./implementation/MidaEntryFactory');


export default function (group, element, translate, elementRegistry)
{
  // Id
  if (is(element, 'bpmn:Task'))
  {

    group.entries.push(midaFactory.taskselect(
    {
      id : 'tasktype',
      label : 'Task type',
      modelProperty : 'tasktype',
      selectOptions: [
      	  {value:"a",name:"Atomic"},{value:"na_c",name:"Not Atomic Concurrent"},{value:"na_nc",name:"Not Atomic Not Concurrent"}
      	],
        emptyParameter: false
    }));
  }
}
