import {
  assign
} from 'min-dash';

import inherits from 'inherits';

import BpmnElementFactory from 'bpmn-js/lib/features/modeling/ElementFactory';
import {
  DEFAULT_LABEL_SIZE
} from 'bpmn-js/lib/util/LabelUtil';
import { getBusinessObject } from 'bpmn-js/lib/util/ModelUtil';


/**
 * A custom factory that knows how to create BPMN _and_ custom elements.
 */
export default function CustomElementFactory(bpmnFactory, moddle, commandStack) {
  BpmnElementFactory.call(this, bpmnFactory, moddle);
  

  
  var self = this;

  /**
   * Create a diagram-js element with the given type (any of shape, connection, label).
   *
   * @param  {String} elementType
   * @param  {Object} attrs
   *
   * @return {djs.model.Base}
   */
  this.create = function(elementType, attrs) {
    var type = attrs.type;

    if (elementType === 'label') {
      return self.baseCreate(elementType, assign({ type: 'label' }, DEFAULT_LABEL_SIZE, attrs));
    }

    if(type === 'bpmn:DataObjectReference' && (attrs.montype === 'input' || attrs.montype === 'output')) {
      let personel = self.createBpmnElement(elementType, attrs)
      
      
      //console.log(dataInputModdle);
      /*
      DataObject
      The Data Object class is an item-aware element. Data Object elements MUST be contained
       within Process or Sub-Process elements. Data Object elements are visually displayed on a Process diagram. Data Object References are a way to reuse Data Objects in the same diagram. They can specify different states of the same Data Object at
      different points in a Process. Data Object Reference cannot specify item definitions, and Data Objects cannot specify states. The names of Data Object References are derived by concatenating the name of the referenced Data Object the state of the Data Object Reference in square brackets as follows: <Data Object Name> [ <DataObject Reference State> ].
      */
      commandStack.execute('element.updateProperties', {
        element:personel,
        properties: {
          datatype: attrs.montype
        }
      })
      
      /*
      if(attrs.montype === 'input') {
        let DataInputSet = moddle.create('mida:DataInputSet');
        // last checked attribute is not generated on the xml output this was just to test the feature 
        DataInputSet.lastChecked = new Date().toString();
        DataInputSet.inputs = [];

        getBusinessObject(personel).get('values').push(DataInputSet)
      } else {
        let DataOutputSet = moddle.create('mida:DataOutputSet');
        // last checked attribute is not generated on the xml output this was just to test the feature 
        DataOutputSet.lastChecked = new Date().toString();
        DataOutputSet.outputs = [];
        
        getBusinessObject(personel).get('values').push(DataOutputSet)        
      }
      */
     
      return personel
    }



    return self.createBpmnElement(elementType, attrs);
  };
}

inherits(CustomElementFactory, BpmnElementFactory);

CustomElementFactory.$inject = [
  'bpmnFactory',
  'moddle',
  'commandStack'
];


/**
 * Returns the default size of custom shapes.
 *
 * The following example shows an interface on how
 * to setup the custom shapes's dimensions.
 *
 * @example
 *
 * var shapes = {
 *   triangle: { width: 40, height: 40 },
 *   rectangle: { width: 100, height: 20 }
 * };
 *
 * return shapes[type];
 *
 *
 * @param {String} type
 *
 * @return {Dimensions} a {width, height} object representing the size of the element
 */
CustomElementFactory.prototype._getCustomElementSize = function(type) {
  var shapes = {
    __default: { width: 100, height: 80 },
    'custom:triangle': { width: 40, height: 40 },
    'custom:circle': { width: 140, height: 140 }
  };

  return shapes[type] || shapes.__default;
};
