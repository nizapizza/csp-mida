'use strict';
var is = require('../../util/ElementHelper').is;



var domify = require('min-dom/dist').domify,
    domClasses = require('min-dom/dist').classes,
    domEvent = require('min-dom/dist').event,
    domQuery = require('min-dom/dist').query;

var events = require('../../util/EventHelper'),
    TOGGLE_MODE_EVENT = events.TOGGLE_MODE_EVENT,
    DATA_UPDATE_EVENT = events.DATA_UPDATE_EVENT,
    GENERATE_TOKEN_EVENT = events.GENERATE_TOKEN_EVENT,
    RESET_SIMULATION_EVENT = events.RESET_SIMULATION_EVENT,
    TERMINATE_EVENT = events.TERMINATE_EVENT,
    CONSUME_TOKEN_EVENT = events.CONSUME_TOKEN_EVENT,
    SHOW_INSTANCE_DATA = events.SHOW_INSTANCE_DATA;
var dataStructure = require('../data/Data');


function DataPanel(eventBus, canvas, selection, contextPad) {
  var self = this;

  this._eventBus = eventBus;
  this._canvas = canvas;
  this._selection = selection;
  this._contextPad = contextPad;
  self._init();
  eventBus.on('import.done', function() {
    self.canvasParent =  self._canvas.getContainer().parentNode;
    self.palette = domQuery('.djs-palette', self._canvas.getContainer());
    self._init();
  });
}

DataPanel.prototype._init = function() {

  	// var prefixHTML = '<div class="data-perspective" id="data-perspective=" style="display:none">';
  	// var selectHTML='';
    // var suffixHTML ='</div>';
    // if(this.dataContainer){
    //   this.dataContainer.innerHTML = selectHTML;
    // }else{
    //     this.dataContainer = domify(prefixHTML+selectHTML+suffixHTML);
    //     //domEvent.bind(this.dataContainer, 'click', this.dataMode.bind(this));
    //     this._canvas.getContainer().appendChild(this.dataContainer);
    // }
};



DataPanel.prototype.dataMode = function(processInstancesWithParent, instanceScope) {

  var globalHTML = "";
  processInstancesWithParent.forEach(function(processInstance) {
    var instance = processInstance.processInstanceId;
    var scope = instanceScope.get(instance);
    var variables = scope.names;
	  var selectHTML = "";
    selectHTML='<div id="instanceData" class="label" title="View Data of Instance '+instance+'">Instance '+instance+' of '+processInstance.parent.businessObject.name
	  selectHTML+='<table id="selectedData" class="selectedData">';
    for(var i = 0; i < variables.length; i++) {
      var value = scope.eval(variables[i]);
      if(!value || value.length == 0){
        value = "---";
      }
      if(variables[i]==='instance'){
        //INSTANCE DO NOT HAS TO BE SHOWN
      }else{
        selectHTML += "<tr><td>"+ variables[i] + "</td><td>" +JSON.stringify(value) + "</td>";
      }
    }
	  selectHTML += "</table><hr></div>";
    globalHTML += selectHTML
  });

  document.getElementById('data-perspective').innerHTML = globalHTML;
	};



DataPanel.$inject = [ 'eventBus', 'canvas', 'selection', 'contextPad' ];

module.exports = DataPanel;
