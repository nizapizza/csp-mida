'use strict';

var is = require('../../../util/ElementHelper').is;

var events = require('../../../util/EventHelper'), CONSUME_TOKEN_EVENT = events.CONSUME_TOKEN_EVENT, GENERATE_TOKEN_EVENT = events.GENERATE_TOKEN_EVENT;

var tokens = require('../TokenSimulationBehavior');

function InclusiveGatewayHandler(animation, eventBus) {
	this._animation = animation;
	this._eventBus = eventBus;
};


InclusiveGatewayHandler.prototype.consume = function(context) {
	var element = context.element, processInstanceId = context.processInstanceId;
	if (!element.tokenCount) {
		element.tokenCount = {};
	}

	if (!element.tokenCount[processInstanceId]) {
		element.tokenCount[processInstanceId] = 0;
	}

	element.tokenCount[processInstanceId]++;

	var self = this;
	var outgoingSequenceFlows = element.outgoing.filter(function(outgoing) {
		return is(outgoing, 'bpmn:SequenceFlow');
	});
	var incomingSequenceFlows = element.incoming.filter(function(incoming) {
		return is(incoming, 'bpmn:SequenceFlow');
	});
	if (Object.keys(outgoingSequenceFlows).length === 1) {

		var isActive = true;

		// DEFINIRE ISACTIVE

		var tokenDistribution = tokens.tokenDistribution.get(processInstanceId);
		// remove token to each incoming sequence flow
		var incomingSequenceFlows = element.incoming.filter(function(incoming) {
			return is(incoming, 'bpmn:SequenceFlow');
		});
		var xml;
		var viewer = window.viewer;
		if (!viewer) {
			viewer = window.modeler;
		}
		viewer.moddle.toXML(viewer.definitions, {
			format : true
		}, function(err, updatedXML) {
			xml = updatedXML;
		});
		var orJoin = element.businessObject.id;
		var tokensDist = "";
		var count = false;
		tokenDistribution.forEach(function(value, key, map) {
			if (tokens.tokenDistribution.get(key) > 0) {
				if (count === true) {
					tokensDist += ";" + key.businessObject.id;
				} else {
					tokensDist += "" + key.businessObject.id;
					count = true;
				}
			}
		});
		var parameters = xml + "%%%" + orJoin + "%%%" + tokensDist;
		// CALL external Service
		var xhttp = new XMLHttpRequest();
		var result;
		var tokCount = element.tokenCount[processInstanceId];
		function ciao() {
			if (this.readyState == 4 && this.status == 200) {
				result = this.responseText;
				if (result === "true" && tokCount === 1) {// ISACTIVE
					// remove token to each incoming sequence flow
					incomingSequenceFlows.forEach(function(incoming) {
						tokenDistribution.set(incoming, 0);
					});
					self._eventBus.fire(GENERATE_TOKEN_EVENT, context);
					element.tokenCount[processInstanceId] = 0;
				}
				if(result === "false"){element.tokenCount[processInstanceId]--;}
			} else {
				alert("Web Service Error: " + this.statusText);
			}

		}
		xhttp.onload = ciao;
		xhttp.open("POST",
				"http://localhost:8080/TokenSimulator/rest/BPMN/OrJoin", true);
		xhttp.send(parameters);

	} else {
		// remove token to each incoming sequence flow

		incomingSequenceFlows.forEach(function(incoming) {
			if (!tokenDistribution.get(incoming)) {
				tokenDistribution.set(incoming, 0);
			} else {
				var count = tokenDistribution.get(incoming) - 1;
				tokenDistribution.set(incoming, count);

			}
		});
		element.tokenCount[processInstanceId] = 0;
		this._eventBus.fire(GENERATE_TOKEN_EVENT, context);
	}
};

InclusiveGatewayHandler.prototype.generate = function(context) {
	var self = this;

	var element = context.element, processInstanceId = context.processInstanceId;
	var tokenDistribution = tokens.tokenDistribution.get(processInstanceId);
	var outgoingSequenceFlows = element.outgoing.filter(function(outgoing) {
		return is(outgoing, 'bpmn:SequenceFlow');
	});
	var outgoingSequenceFlows = element.outgoing.filter(function(outgoing) {
		return is(outgoing, 'bpmn:SequenceFlow');
	});
	if (Object.keys(outgoingSequenceFlows).length === 1) {
		outgoingSequenceFlows.forEach(function(outgoing) {
			if (!tokenDistribution.get(outgoing)) {
				tokenDistribution.set(outgoing, 1);
			} else {
				var count = tokenDistribution.get(outgoing);
				tokenDistribution.set(outgoing, count++);
			}
			self._animation.createAnimation(outgoing, processInstanceId,
					function() {
						self._eventBus.fire(CONSUME_TOKEN_EVENT, {
							element : outgoing.target,
							processInstanceId : processInstanceId
						});
					});

		});
	} else {
		outgoingSequenceFlows.forEach(function(outgoing) {
			if ((outgoing.businessObject.conditionExpression && window
					.eval(outgoing.businessObject.conditionExpression.body))) {
				if (!tokenDistribution.get(outgoing)) {
					tokenDistribution.set(outgoing, 1);
				} else {
					var count = tokenDistribution.get(outgoing);
					tokenDistribution.set(outgoing, count++);
				}
				self._animation.createAnimation(outgoing, processInstanceId,
						function() {
							self._eventBus.fire(CONSUME_TOKEN_EVENT, {
								element : outgoing.target,
								processInstanceId : processInstanceId
							});
						});
			}

		});
	}

};

InclusiveGatewayHandler.$inject = [ 'animation', 'eventBus' ];

module.exports = InclusiveGatewayHandler;