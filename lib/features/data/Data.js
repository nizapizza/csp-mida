'use strict';

var elementHelper = require('../../util/ElementHelper'), getBusinessObject = elementHelper.getBusinessObject, is = elementHelper.is;
var Scope = require('./Scope');
var events = require('../../util/EventHelper'),  GENERATE_TOKEN_EVENT = events.GENERATE_TOKEN_EVENT, CONSUME_TOKEN_EVENT = events.CONSUME_TOKEN_EVENT;
var forEach = require('lodash').forEach;

var init = false;
var self;
function Data(eventBus, animation, elementRegistry, log, elementNotifications,
		canvas, processInstances) {
 self = this;
	this._animation = animation;
	this._elementRegistry = elementRegistry;
	this._log = log;
	this._elementNotifications = elementNotifications;
	this._canvas = canvas;
	this._processInstances = processInstances;
	this._eventBus = eventBus;
}
function initialize(){
	init = true;
}


function evaluateDataObjects(element, scope){
	var dObjects = modeler.get('elementRegistry').filter(function(el) {
		return is(el, 'bpmn:DataObjectReference') && element.parent === el.parent;
	});
	for(var k = 0; k<dObjects.length; k++){
		if(dObjects[k].businessObject.extensionElements){
			forEach(dObjects[k].businessObject.extensionElements.values, function(v) {
				forEach(v.values, function(value) {
					if(value.dataField){
							var res = value.dataField;
							var assign = res.split("=");
							if(assign.length>1){//is assignement
								assign[0] = assign[0].replace('var', '');
								assign[0] = assign[0].replace(/\s/g, '');
								if(assign[0] != ""){
									Scope.addVariables(scope, [assign[0]]);
									scope.eval(assign[0]+" = "+assign[1]);
								}
							}else{
								res = res.replace('var', '');
								res = res.replace(/\s/g, '');
								if(res != ""){
									Scope.addVariables(scope, [res]);;
								}
							}
					}
				});
			});
		}
	}
};

function evaluateGuard(activity, scope){
	var guard = activity.businessObject.get('guard');
	if(guard===undefined){
		return true;
	}
	return scope.eval(guard);
};

function evaluateCondition(condition, scope){
	return scope.eval(condition);
};


function evaluateAssignments(activity, scope){
	if(activity.businessObject.extensionElements){
		forEach(activity.businessObject.extensionElements.values, function(v) {
			forEach(v.values, function(value) {
				if(value.assignment){
					var assignment = value.assignment;

					var assign = assignment.split("=");
					if(assign.length>1 && !(assign[0].includes('[') && assign[0].includes(']'))){//is assignement
						assign[0] = assign[0].replace('var', '');
						assign[0] = assign[0].replace(/\s/g, '');
						if(assign[0] != ""){
							var unk = assign[0];
							assign.shift();
			 				var val = assign.join('=');
							Scope.addVariables(scope, [unk]);
							scope.eval(unk+" = "+val);
						}
					}else{
						assignment = assignment.replace('var', '');
						assignment = assignment.replace(/\s/g, '');
						if(assignment != ""){
							scope.eval(assignment);
						}
					}
				}
			});
		});
	}
};

function prepareMessage(element, scope){
	if(element.businessObject.extensionElements){
		var msg = [];
		forEach(element.businessObject.extensionElements.values, function(v) {
			forEach(v.values, function(value) {
				if(value.field){
						var tmp =scope.eval(value.field);
						if(typeof tmp === 'string' && (tmp.includes("\'") || tmp.includes("\""))){
							tmp = tmp.replace(/\'/g, '')
							tmp = tmp.replace(/\"/g, '')
						}
						if(value.field === tmp){ //Costant field
							msg.push(messageFields[i]);
						}else {
							msg.push(tmp);
						}

				}
			});
		});
			return msg;
	}
};

function prepareTemplate(templ){
	var fields = [], corr = [];
	forEach(templ, function(v) {
		forEach(v.values, function(value) {
			if(value.field){
				fields.push(value.field);
				corr.push(value.isCorrelation);
			}
		});
	});
	return [fields, corr];
}


function patterMatching(msg, templ){
	var template = templ.template[0];
	var isCorrelation = templ.template[1];
	var message = msg; //Already evalued
	var scope = templ.scope;
	if( (template.length - message.length )!= 0){
		console.log('Message lenght != Template length')
		return false;
	}
	var operations = [];
	var match = true;
	for(var i = 0; i<template.length; i++){

		var field = template[i];
		if(isCorrelation[i]){ //patterMatching field
			console.log([scope.eval(field), message[i]])
			if(scope.eval(field)!=message[i]){
				console.log('NO patterMatching')
				match = false;
			}
		}else{
			if(typeof message[i] === 'string'){
				operations.push(field +" = '"+message[i]+"'");
			}else if(typeof message[i] === 'boolean' || typeof message[i] === 'number'){
				operations.push(field +" = "+message[i]);
			}
			else{
				if(message[i].length >1 && typeof message[i][0] === 'string'){
					for(var k =0; k<message[i].length; k++){
						message[i][k] = "'"+message[i][k]+"'";
					}
				}
				operations.push(field +" = ["+message[i]+"]");
			}
		}
	}
	if (!match){
		return false;
	}
	operations.forEach(function(op){
		scope.eval(op)
	});
	console.log('YES patterMatching')
	return true;
};

Data.$inject = [ 'eventBus', 'animation', 'elementRegistry', 'log',
		'elementNotifications', 'canvas', 'processInstances' ];

module.exports = Data;
module.exports.evaluateCondition = evaluateCondition;
module.exports.evaluateDataObjects = evaluateDataObjects;
module.exports.evaluateGuard = evaluateGuard;
module.exports.evaluateAssignments = evaluateAssignments;
module.exports.prepareMessage = prepareMessage;
module.exports.prepareTemplate = prepareTemplate;
module.exports.patterMatching = patterMatching;
