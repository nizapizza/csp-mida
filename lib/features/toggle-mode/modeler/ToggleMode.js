'use strict';
var is = require('../../../util/ElementHelper').is;

var domify = require('min-dom/dist').domify,
    domClasses = require('min-dom/dist').classes,
    domEvent = require('min-dom/dist').event,
    domQuery = require('min-dom/dist').query;

var events = require('../../../util/EventHelper'),
    TOGGLE_MODE_EVENT = events.TOGGLE_MODE_EVENT,
    GENERATE_TOKEN_EVENT = events.GENERATE_TOKEN_EVENT,
    RESET_SIMULATION_EVENT = events.RESET_SIMULATION_EVENT,
    TERMINATE_EVENT = events.TERMINATE_EVENT,
    CONSUME_TOKEN_EVENT = events.CONSUME_TOKEN_EVENT;
var dataStructure = require('../../data/Data');
var getBusinessObject = require('bpmn-js/lib/util/ModelUtil').getBusinessObject;

function ToggleMode(eventBus, canvas, selection, contextPad, processInstances) {
  var self = this;

  this._eventBus = eventBus;
  this._canvas = canvas;
  this._selection = selection;
  this._contextPad = contextPad;
this._processInstances = processInstances;
  this.simulationModeActive = false;
  this.dataModeActive = false;
  eventBus.on('import.done', function() {
    self.canvasParent =  self._canvas.getContainer().parentNode;
    self.palette = domQuery('.djs-palette', self._canvas.getContainer());
    self._init();
  });
  eventBus.on(RESET_SIMULATION_EVENT, function() {
      console.log(self);
      self.resetData();
  });
  eventBus.on(TERMINATE_EVENT, function(context) {
    self.resetData();
  });
}

ToggleMode.prototype._init = function() {


    if(this.container){
      this.container.innerHTML = 'Animation Mode <span class="toggle"><i class="fa fa-toggle-off"></i></span>';
    }else{
      this.container = domify(`
        <div class="toggle-mode">
          Animation Mode <span class="toggle"><i class="fa fa-toggle-off"></i></span>
        </div>
        `);
        domEvent.bind(this.container, 'click', this.toggleMode.bind(this));

        this._canvas.getContainer().appendChild(this.container);
    }
};



ToggleMode.prototype.toggleMode = function() {
  if (this.simulationModeActive) {
    this.container.innerHTML = 'Animation Mode <span class="toggle"><i class="fa fa-toggle-off"></i></span>';

    domClasses(this.canvasParent).remove('simulation');
    domClasses(this.palette).remove('hidden');

    this._eventBus.fire(TOGGLE_MODE_EVENT, {
      simulationModeActive: false
    });

    var elements = this._selection.get();

    if (elements.length === 1) {
      this._contextPad.open(elements[0]);
    }
    document.getElementById("save").style.display = "block";
    document.getElementById("load").style.display = "block";
  } else {
    this.container.innerHTML = '<span class="toggle"><i class="fa fa-toggle-on"></i></span>';

    document.getElementById("js-properties-panel").style.display = "none";
    document.getElementById("save").style.display = "none";
    document.getElementById("load").style.display = "none";
    domClasses(this.canvasParent).add('simulation');
    domClasses(this.palette).add('hidden');

    this._eventBus.fire(TOGGLE_MODE_EVENT, {
      simulationModeActive: true
    });
  }

  this.simulationModeActive = !this.simulationModeActive;
};



ToggleMode.$inject = [ 'eventBus', 'canvas', 'selection', 'contextPad' ];

module.exports = ToggleMode;
