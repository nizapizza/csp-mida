import inherits from 'inherits';

import ContextPadProvider from 'bpmn-js/lib/features/context-pad/ContextPadProvider';
var is = require('../../../util/ElementHelper').is;
import {
  isAny
} from 'bpmn-js/lib/features/modeling/util/ModelingUtil';

import {
  assign,
  bind
} from 'min-dash';



export default function CustomContextPadProvider(injector, connect, translate) {
  var contextPad = this._contextPad,
       modeling = this._modeling,

       elementFactory = this._elementFactory,
       connect = this._connect,
       create = this._create,
       popupMenu = this._popupMenu,
       canvas = this._canvas,
       rules = this._rules,
       autoPlace = this._autoPlace,
       translate = this._translate;

   var actions = {};

   if (element.type === 'label') {
     return actions;
   }

   var businessObject = element.businessObject;

   function startConnect(event, element) {
     connect.start(event, element);
   }

   function removeElement(e) {
     modeling.removeElements([ element ]);
   }

   function getReplaceMenuPosition(element) {

     var Y_OFFSET = 5;

     var diagramContainer = canvas.getContainer(),
         pad = contextPad.getPad(element).html;

     var diagramRect = diagramContainer.getBoundingClientRect(),
         padRect = pad.getBoundingClientRect();

     var top = padRect.top - diagramRect.top;
     var left = padRect.left - diagramRect.left;

     var pos = {
       x: left,
       y: top + padRect.height + Y_OFFSET
     };

     return pos;
   }


   /**
    * Create an append action
    *
    * @param {String} type
    * @param {String} className
    * @param {String} [title]
    * @param {Object} [options]
    *
    * @return {Object} descriptor
    */
   function appendAction(type, className, title, options) {

     if (typeof title !== 'string') {
       options = title;
       title = translate('Append {type}', { type: type.replace(/^bpmn:/, '') });
     }

     function appendStart(event, element) {

       var shape = elementFactory.createShape(assign({ type: type }, options));
       create.start(event, shape, element);
     }


     var append = autoPlace ? function(event, element) {
       var shape = elementFactory.createShape(assign({ type: type }, options));

       autoPlace.append(element, shape);
     } : appendStart;


     return {
       group: 'model',
       className: className,
       title: title,
       action: {
         dragstart: appendStart,
         click: append
       }
     };
   }

   function splitLaneHandler(count) {

     return function(event, element) {
       // actual split
       modeling.splitLane(element, count);

       // refresh context pad after split to
       // get rid of split icons
       contextPad.open(element, true);
     };
   }
  injector.invoke(ContextPadProvider, this);

  var cached = bind(this.getContextPadEntries, this);

  this.getContextPadEntries = function(element) {
    var actions = cached(element);
console.log(element)
    var businessObject = element.businessObject;

    function startConnect(event, element, autoActivate) {
      connect.start(event, element, autoActivate);
    }

    if (is(element, 'bpmn:DataObjectReference')) {
      assign(actions, {
        'replace': {
          group: 'edit',
          className: 'bpmn-icon-screw-wrench',
          title: translate('Change type'),
          action: {
            click: function(event, element) {

              var position = assign(getReplaceMenuPosition(element), {
                cursor: { x: event.x, y: event.y }
              });

              popupMenu.open(element, 'bpmn-replace', position);
            }
          }
        }
      });
    }

    return actions;
  };
}

inherits(CustomContextPadProvider, ContextPadProvider);

CustomContextPadProvider.$inject = [
  'injector',
  'connect',
  'translate'
];
