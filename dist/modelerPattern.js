'use strict';

var fs = require('fs');
var tokenSimulation = require('../lib/modeler');
var param;
var pos = document.URL.indexOf("?");
param=document.URL.substring(pos+1);
var exampleXML;
switch (param)
{
  case "0":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/SendPattern.bpmn', 'utf-8');
  break;
  case "1":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/ReceivePattern.bpmn', 'utf-8');
  break;
  case "2":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/SendReceivePattern.bpmn', 'utf-8');
  break;
  case "3":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/RacingIncominPatternA.bpmn', 'utf-8');
  break;
  case "4":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/RacingIncominPatternB.bpmn', 'utf-8');
  break;
  case "5":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/OneToManySendPatternA.bpmn', 'utf-8');
  break;
  case "6":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/OneFromManyReceivePatternA.bpmn', 'utf-8');
  break;
  case "7":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/OneToManySendReceivePattern.bpmn', 'utf-8');
  break;
  case "8":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/RequestWithReferralPattern.bpmn', 'utf-8');
  break;
  case "9":
  exampleXML = fs.readFileSync(__dirname + '/resources/models/Pattern/RequestWithRelayPattern.bpmn', 'utf-8');
  break;
  default:
  alert('Default case');
}
var BpmnModeler = require('bpmn-js/lib/Modeler');

//var propertiesPanelModule = require('bpmn-js-properties-panel'),
//propertiesProviderModule = require('bpmn-js-properties-panel/lib/provider/camunda');
var propertiesPanelModule = require('../lib/features/data-properties-panel'),
propertiesProviderModule = require('../lib/features/data-properties-panel/lib/provider/camunda');
var camundaModdleDescriptor = require('camunda-bpmn-moddle/resources/camunda');
var _ = require('lodash');
var ModdleXMLW = require('moddle-xml').Writer;
var BpmnModdle = require('bpmn-moddle');

var moddle = new BpmnModdle();

var modeler = new BpmnModeler({
  container: '#canvas',
  additionalModules: [
    tokenSimulation,
    propertiesPanelModule,
    propertiesProviderModule
  ],
  propertiesPanel: {
    parent: '#js-properties-panel'
  },
  keyboard: {
    bindTo: document
  },
  moddleExtensions: {
    camunda: camundaModdleDescriptor
  }
});

modeler.importXML(exampleXML, function(err) {
  if (!err) {
    modeler.get('canvas').zoom('fit-viewport');
  } else {
    console.log('something went wrong:', err);
  }
});
function nuovo(xml) {
  modeler.importXML(xml, function(err) {
    if (!err) {
      modeler.get('canvas').zoom('fit-viewport');
    } else {
      sessionStorage.clear()
      console.log('something went wrong:', err);
    }
  });
}

function aggiorna(xml) {
  var writer = new ModdleXMLW({
    format: false,
    preamble: false
  });

  sessionStorage.setItem('ultimoDiagramma', JSON.stringify(writer.toXML(xml)))

  setTimeout(function() {
    location.reload()
  }, 500)

}

if (sessionStorage.getItem('ultimoDiagramma')) {
  nuovo(JSON.parse(sessionStorage.getItem('ultimoDiagramma')))
} else {
  nuovo(exampleXML)
}

$('#procediSalva').hide();
$('#opzioni').hide();

var eventBus = modeler.get('eventBus');

eventBus.on('element.dblclick', function(e) {
  $('#qmassimo').val("");
  $('#qminimo').val("");

  var inizio = modeler.get('canvas').getRootElement().businessObject.$parent;

  var elementiInTavola = modeler.get('canvas').getRootElement().children;

  if (e.element.type === "bpmn:Participant") {
    $('#lanciaDialog').click()

    var M = false;
    var K = false;
    var B = moddle.create('bpmn:ParticipantMultiplicity', {
      maximum: 0,
      minimum: 0
    });

    _.each(inizio.rootElements[0].participants, function(v, k) {
      if (v.id == e.element.id) {
        M = v
        K = k
      }
    })

    if (e.element.businessObject.hasOwnProperty("participantMultiplicity")) {
      $('#abilitaB').prop('checked', true);
      $('#procediSalva').text("Update Values")
      $('#qmassimo').val(M.participantMultiplicity.maximum);
      $('#qminimo').val(M.participantMultiplicity.minimum);
    } else {
      M.participantMultiplicity = B;
      $('#procediSalva').text("Aggiungi")
    }

    if ($("#abilitaB").is(":checked")) {
      $('#procediSalva').show();
      $('#opzioni').show();
    }

    $(document).on('change', '#abilitaB', function() {
      if ($(this).is(":checked")) {
        $('#procediSalva').show();
        $('#opzioni').show();
      } else {
        $('#procediSalva').hide();
        $('#opzioni').hide();
        delete M.participantMultiplicity
        aggiorna(inizio)
      }
    })


    $('#procediSalva').click(function(e) {
      e.preventDefault()

      M.participantMultiplicity.maximum = parseInt($('#qmassimo').val())
      M.participantMultiplicity.minimum = parseInt($('#qminimo').val())
      if( parseInt($('#qminimo').val()) > parseInt($('#qmassimo').val()) ){
        alert("Il valore minimo supera quello massimo");
      }else if((parseInt($('#qminimo').val())<0) || (parseInt($('#qmassimo').val()))<0){
        alert("I valori scelti non possono essere negativi");
      }else if(($('#qminimo').val()=="") || ($('#qmassimo').val())==""){
        alert("Inserire un valore!");
      }else{
        aggiorna(inizio)
      }
    })

  }
});

window.modeler = modeler;
