// FUTURE BUG FIX, REMOVE THE DROP THAT ALLOWS US TO CHOOSE BETWEEN INPUT AND OUTPUT, THAT ONE CAUSES BUGS
// MAKE SURE TO REMOVE IT


// TODO the property bug


// TODO unconnected stuff



// STEPS FOR IMPORT

// maintain tasks


// rename the DataObjectReference to DataObject, remove some of its attributes and fix the DataAssociation it has

// when fixing the DataAssociations, the Inputs and Outputs are handled differently. we check the blacklist and fix the stuff

// remove the DataObject and DataObjectReferences if there are no inputs or outputs

// Change the Shapes !!!!

// create new DataAssociations / manipulate them big / small inputs 

// create encapsulative IO is necessary for export


// this is the function where we remove the IO specification tags, we first take the xml string


let normal_object_list = []
let all_outputs = []
let all_inputs = []



function deleteObjects(xmlDoc) {

    let object_tags = xmlDoc.getElementsByTagName('bpmn:dataObject')

    while (object_tags.length > 0) {
        object_tags[0].remove()
    }
}


function deleteIOSpecificationTags(xmlDoc) {
    // check on import if there is any tags with IOSpecification, if yes, remove them completely

    let iospecification_tags = xmlDoc.getElementsByTagName('bpmn:ioSpecification')

    // the length of the ioSpecification tags changes so we keep removing it
    while (iospecification_tags.length > 0) {
        iospecification_tags[0].remove();
    }

}


function extractNormalDataObjReferences(xmlDoc) {

    let ioSpecs = xmlDoc.getElementsByTagName('bpmn:ioSpecification')


    for (let i = 0; i < ioSpecs.length; i++) {
        const element = ioSpecs[i];

        // if null, then this is the process level IOSpecification
        if (element.parentNode.tagName === 'bpmn:process') {
            for (let i = 0; i < element.children.length; i++) {
                const childz = element.children[i];
                if (!normal_object_list.includes(childz.id)) {
                    normal_object_list.push(childz.id)
                }
            }
        }
    }
}


function handleImportXml(xml) {

    let parser = new DOMParser()
    // we parse the xml string given to us, xml => document
    let xmlDoc = parser.parseFromString(xml, "text/xml")


    deleteObjects(xmlDoc)

    extractNormalDataObjReferences(xmlDoc)

    deleteIOSpecificationTags(xmlDoc)

    // WE NEED TO HAVE A PROPERTY
    // for each process
    let process = xmlDoc.getElementsByTagName('bpmn:process')
    for (let i = 0; i < process.length; i++) {
        const proc = process[i];


        // looking for the task
        for (let i = 0; i < proc.children.length; i++) {
            const child = proc.children[i];


            // looking for the task's children, like dataAssociations
            for (let i = 0; i < child.children.length; i++) {
                const plausible = child.children[i]


                if (plausible.tagName === 'bpmn:dataOutputAssociation') {


                    let targetRef = ''
                    let sourceRef = ''

                    for (let i = 0; i < plausible.children.length; i++) {

                        const plau = plausible.children[i];

                        if (plau.tagName === 'bpmn:sourceRef') {
                            sourceRef = plau.innerHTML
                        }

                        if (plau.tagName === 'bpmn:targetRef') {
                            targetRef = plau.innerHTML
                        }

                    }

                    //console.log('sourceRef ' + sourceRef);
                    //console.log('targetRef ' + targetRef);

                    if (!all_inputs.includes(targetRef) && !all_outputs.includes(targetRef)) {

                        all_outputs.push(targetRef)

                        let dataObjectID = generateID()

                        // create a random DataObject
                        let dataObject = xmlDoc.createElement("bpmn:dataObject")
                        dataObject.setAttribute("id", "DataObject_" + dataObjectID)

                        // create a DataObjectReference and a DataObject
                        let dataObjectReference = xmlDoc.createElement("bpmn:dataObjectReference")
                        dataObjectReference.setAttribute("id", targetRef)
                        dataObjectReference.setAttribute("dataObjectRef", "DataObject_" + dataObjectID)

                        // check first of all if there is a DataObjReference as a dataobject in the sourceRef
                        // if yes, this means, this is not an input, but just a normal Data Obj Ref

                        for (let i = 0; i < normal_object_list.length; i++) {
                            const potential = normal_object_list[i]
                            if (potential === targetRef) {
                                dataObjectReference.setAttribute("datatype", "output")
                            }

                        }

                        child.parentNode.appendChild(dataObjectReference)
                        child.parentNode.appendChild(dataObject)


                        // CHANGE THE SHAPE and swap the oldInput with the DataObjectReference
                        let shapes = xmlDoc.getElementsByTagName('bpmndi:BPMNShape')
                        // loop through shapes
                        for (let i = 0; i < shapes.length; i++) {
                            const shape = shapes[i];
                            let bpmnElement = shape.getAttribute("bpmnElement")
                            if (bpmnElement === targetRef) {
                                shape.setAttribute("bpmnElement", targetRef)
                            }

                        }

                    }

                }


                if (plausible.tagName === 'bpmn:dataInputAssociation') {


                    // extract the value in the id field of the property and put it in the innerHTML src
                    // property[0]

                    let targetRef = ''
                    let sourceRef = ''
                    let property = ''


                    // get the property
                    // lets assume that the bpmn property exists and was not deleted
                    for (let i = 0; i < child.children.length; i++) {
                        const element = child.children[i];
                        if (element.tagName === 'bpmn:property') {
                            property = element.id
                        }
                    }

                    for (let i = 0; i < plausible.children.length; i++) {

                        const plau = plausible.children[i];

                        if (plau.tagName === 'bpmn:sourceRef') {
                            sourceRef = plau.innerHTML
                        }

                        if (plau.tagName === 'bpmn:targetRef') {
                            targetRef = plau.innerHTML
                            plau.innerHTML = property
                        }

                    }

                    if (!all_inputs.includes(sourceRef) && !all_outputs.includes(sourceRef)) {

                        all_inputs.push(sourceRef)

                        let dataObjectID = generateID()

                        // create a random DataObject
                        let dataObject = xmlDoc.createElement("bpmn:dataObject")
                        dataObject.setAttribute("id", "DataObject_" + dataObjectID)


                        // create a DataObjectReference and a DataObject
                        let dataObjectReference = xmlDoc.createElement("bpmn:dataObjectReference")
                        dataObjectReference.setAttribute("id", sourceRef)
                        dataObjectReference.setAttribute("dataObjectRef", "DataObject_" + dataObjectID)


                        // check first of all if there is a DataObjReference as a dataobject in the sourceRef
                        // if yes, this means, this is not an input, but just a normal Data Obj Ref

                        for (let i = 0; i < normal_object_list.length; i++) {
                            const potential = normal_object_list[i]
                            if (potential === sourceRef) {
                                dataObjectReference.setAttribute("datatype", "input")
                            }

                        }


                        child.parentNode.appendChild(dataObjectReference)
                        child.parentNode.appendChild(dataObject)

                        console.log(plausible);

                        // CHANGE THE SHAPE and swap the oldInput with the DataObjectReference
                        let shapes = xmlDoc.getElementsByTagName('bpmndi:BPMNShape')
                        // loop through shapes
                        for (let i = 0; i < shapes.length; i++) {
                            const shape = shapes[i];
                            let bpmnElement = shape.getAttribute("bpmnElement")
                            if (bpmnElement === targetRef) {
                                shape.setAttribute("bpmnElement", targetRef)
                            }

                        }

                    }
                
                }

            }

        }

    }

    console.log(xmlDoc);
    // we convert the xml document back to a string format so it could forward the data flow correctly
    let xmlString = new XMLSerializer().serializeToString(xmlDoc.documentElement);
    return xmlString

}




function generateID() {
    return Math.random().toString(36).substr(2, 7);
}

