function handleProcesses(xmlDoc) {
    // ENCAPSULATE DATA OBJ REFERENCE AND DATA OBJECTS INSIDE A BPMN IOSPECIFICATION FIELD

    let processes = xmlDoc.getElementsByTagName('bpmn:process')

    // FOR EACH PROCESS
    for (let i = 0; i < processes.length; i++) {
        
        let encapsulativeIO = xmlDoc.createElement("bpmn:ioSpecification")

        let listOfElements = []

        const tasks = processes[i].children;

        // FOR EACH TASK
        for (let i = 0; i < tasks.length; i++) {
            const task = tasks[i];
            if ((task.nodeName === 'bpmn:dataObjectReference') || (task.nodeName === 'bpmn:dataObject')) {
                listOfElements.push(task)
            }
        }

        // APPEND EACH ELEMENT TO ITS AN BOX OF IOS THAT WE WILL LATER APPEND TO THE PROCESS
        for (let i = 0; i < listOfElements.length; i++) {
            const element = listOfElements[i];
            encapsulativeIO.appendChild(element)
        }

        processes[i].appendChild(encapsulativeIO)
        console.log(processes[i]);
    }

    /*

    console.log(encapsulativeIO);
    //mofos[tasks].parentNode.appendChild(encapsulativeIO)

    console.log(mofos[tasks].parentNode);
    */
}
