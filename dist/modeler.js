

'use strict';

var tokenSimulation = require('../lib/modeler');

import exampleXML from './resources/models/runningExample.bpmn';

var propertiesPanelModule = require('bpmn-js-properties-panel');

import midaProviderModule from '../lib/features/mida-properties-panel/provider/extension';
import midaModdleDescriptor from '../lib/features/mida-properties-panel/descriptors/mida';

import customElements from '../lib/features/mida-modeler-extension/custom-elements.json';
var BpmnModeler = require('bpmn-js/lib/Modeler').default;
var CustomModeler = require('../lib/features/custom-modeler').default;

var _ = require('lodash');
var ModdleXMLW = require('moddle-xml').Writer;
import BpmnModdle from 'bpmn-moddle';


var	camundaExtensionModule = require('camunda-bpmn-moddle/lib');

var camundaModdle = require('camunda-bpmn-moddle/resources/camunda');

var moddle = new BpmnModdle({ camunda: camundaModdle });

var modeler = new CustomModeler({
  container: '#canvas',
  additionalModules: [
    tokenSimulation,
    midaProviderModule,
    propertiesPanelModule
  ],
  propertiesPanel: {
	    parent: '#js-properties-panel'
	  },
  keyboard: {
    bindTo: document
  },
  moddleExtensions: {
    mida: midaModdleDescriptor,
    camunda: camundaModdle,
  }
});

modeler.importXML(exampleXML, function(err) {
  if (!err) {
    modeler.get('canvas').zoom('fit-viewport');
  } else {
    sessionStorage.clear()
    console.log('something went wrong:', err);
  }
  //  modeler.addCustomElements(customElements);
});

// you may hook into any of the following events
let eventBus = modeler.get('eventBus')

var events = [
  'element.dblclick'
];

events.forEach(function(event) {

  eventBus.on(event, function(e) {
    
    console.log(event, 'on', e.element);
  });
});


//
// function nuovo(xml) {
//   modeler.importXML(xml, function(err) {
//     if (!err) {
//       modeler.get('canvas').zoom('fit-viewport');
//     } else {
//       sessionStorage.clear()
//       console.log('something went wrong:', err);
//     }
//   });
// }
//
// function aggiorna(xml) {
//   var writer = new ModdleXMLW({
//     format: false,
//     preamble: false
//   });
//
//   sessionStorage.setItem('ultimoDiagramma', JSON.stringify(writer.toXML(xml)))
//
//   setTimeout(function() {
//     location.reload()
//   }, 500)
//
// }
//
// if (sessionStorage.getItem('ultimoDiagramma')) {
//   nuovo(JSON.parse(sessionStorage.getItem('ultimoDiagramma')))
// } else {
//   nuovo(exampleXML)
// }
//
// $('#procediSalva').hide();
// $('#opzioni').hide();
//
// var eventBus = modeler.get('eventBus');
//
// eventBus.on('element.dblclick', function(e) {
//   $('#qmassimo').val("");
//   $('#qminimo').val("");
//
//   var inizio = modeler.get('canvas').getRootElement().businessObject.$parent;
//
//   var elementiInTavola = modeler.get('canvas').getRootElement().children;
//
//   if (e.element.type === "bpmn:Participant") {
//     $('#lanciaDialog').click()
//     var M = false;
//     var K = false;
//
//     var B = moddle.create('bpmn:ParticipantMultiplicity', {
//       maximum: 0,
//       minimum: 0
//     });
//
//
//
//     _.each(inizio.rootElements[0].participants, function(v, k) {
//       if (v.id == e.element.id) {
//
//         M = v
//         K = k
//       }
//     })
//
//     if (e.element.businessObject.hasOwnProperty("participantMultiplicity")) {
//       $('#abilitaB').prop('checked', true);
//       $('#procediSalva').text("Update Values")
//       $('#qmassimo').val(M.participantMultiplicity.maximum);
//       $('#qminimo').val(M.participantMultiplicity.minimum);
//     } else {
//       M.participantMultiplicity = B;
//       $('#procediSalva').text("Aggiungi")
//     }
//
//     if ($("#abilitaB").is(":checked")) {
//       $('#procediSalva').show();
//       $('#opzioni').show();
//     }
//
//     $(document).on('change', '#abilitaB', function() {
//       if ($(this).is(":checked")) {
//         $('#procediSalva').show();
//         $('#opzioni').show();
//       } else {
//         $('#procediSalva').hide();
//         $('#opzioni').hide();
//         delete M.participantMultiplicity
//         aggiorna(inizio)
//       }
//     })
//
//
//     $('#procediSalva').click(function(e) {
//       e.preventDefault()
//
//       M.participantMultiplicity.maximum = parseInt($('#qmassimo').val())
//       M.participantMultiplicity.minimum = parseInt($('#qminimo').val())
//       if( parseInt($('#qminimo').val()) > parseInt($('#qmassimo').val()) ){
//        alert("Il valore minimo supera quello massimo");
//      }else if((parseInt($('#qminimo').val())<0) || (parseInt($('#qmassimo').val()))<0){
//        alert("I valori scelti non possono essere negativi");
//      }else if(($('#qminimo').val()=="") || ($('#qmassimo').val())==""){
//        alert("Inserire un valore!");
//      }else{
//       aggiorna(inizio)
//      }
//      })
//
//   }
// });

window.modeler = modeler;
