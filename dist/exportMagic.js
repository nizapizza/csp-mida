// TODO the property bug
// TODO unconnected stuff
// TODO fix ctrl-z 
//TODO fix empty ioSpecification sometimes


// STEPS FOR EXPORT

// create encapsulative IO is necessary for export
// create new DataAssociations / manipulate them big / small inputs 
// Change the Shapes !!!!
// remove the DataObject and DataObjectReferences if there are no inputs or outputs
// rename the DataObjectReference to DataObject, remove some of its attributes and fix the DataAssociation it has
// when fixing the DataAssociations, the Inputs and Outputs are handled differently. we check the blacklist and fix the stuff


// do this in reverse for import
// fck my life



let BlackListID = []
let BlackListDataObjRef = []


// this handles the pre-export of the doc, by looking at tasks and their DataInputAssociations
function handleExportXml(xmlDoc) {


    handleProcesses(xmlDoc)

    deleteDataObjReferences_DataObjects(xmlDoc)

}


function deleteDataObjReferences_DataObjects(xmlDoc) {

    // deletes those tags from the exported XML in order to be compatible with the standard

    let dataObjReferences = xmlDoc.getElementsByTagName('bpmn:dataObjectReference')
    let dataObjects = xmlDoc.getElementsByTagName('bpmn:dataObject')

    for (let i = dataObjReferences.length - 1; i >= 0; --i) {
        // when we find dataObjReferences we should rename them
        if ((dataObjReferences[i].getAttribute("datatype") === 'input') || (dataObjReferences[i].getAttribute("datatype") === 'output')) {
            dataObjReferences[i].remove()
        }
    }

    for (let i = dataObjects.length - 1; i >= 0; --i) {
        dataObjects[i].remove();
    }

}

function handleNormalDataObjectReferences(xmlDoc, element, bpmn_outputSet, bpmn_inputSet, ioSpecification) {

    // if there is no children[1] means this is an DataOutputAssociation of a DataObjectReference
    if (element.parentNode.children[1] === undefined) {
        // we are dealing with an DataInputAssociation of a DataObjReference

        // add bpmn:dataInput
        let dataOutput = xmlDoc.createElement("bpmn:dataOutput")
        // DataOutput_mimpj8h
        let dataOutputID = generateID()

        dataOutput.setAttribute("id", "DataOutput" + dataOutputID)
        dataOutput.setAttribute("name", "Data Output " + dataOutputID)

        // create data Output ref
        let dataOutputRefs = xmlDoc.createElement("bpmn:dataOutputRefs")
        // need to figure this one later
        dataOutputRefs.innerHTML = "DataOutput" + dataOutputID


        //add the source element in the dataOutputAssociation
        //<bpmn2:sourceRef>DataOutput_1</bpmn2:sourceRef>
        let sourceRef = xmlDoc.createElement("bpmn:sourceRef")
        sourceRef.innerHTML = "DataOutput" + dataOutputID

        element.parentNode.appendChild(sourceRef)

        // we change the target to point to the internal dataOutput we will create and insert into the IOSpecification
        bpmn_outputSet.appendChild(dataOutputRefs)
        ioSpecification.appendChild(dataOutput)


    } else {

        // we are dealing with an DataInputAssociation of a DataObjReference

        // add bpmn:dataInput
        let dataInput = xmlDoc.createElement("bpmn:dataInput")
        // DataInput_mimpj8h
        let dataInputID = generateID()

        dataInput.setAttribute("id", "DataInput" + dataInputID)
        dataInput.setAttribute("name", "Data Input " + dataInputID)

        // create data input ref
        let dataInputRefs = xmlDoc.createElement("bpmn:dataInputRefs")
        // need to figure this one later
        dataInputRefs.innerHTML = "DataInput" + dataInputID

        // fix the target of the standard DataObjectReference
        element.parentNode.children[1].innerHTML = "DataInput" + dataInputID

        // we change the target to point to the internal dataInput we will create and insert into the IOSpecification
        bpmn_inputSet.appendChild(dataInputRefs)
        ioSpecification.appendChild(dataInput)

    }

}



function generateID() {
    return '_' + Math.random().toString(36).substr(2, 7);
}

function xmlMagic(xmlDoc, mofos, dataAssociations) {
    //InputSet_2
    //Input Set 2
    let ioSpecification = xmlDoc.createElement("bpmn:ioSpecification")
    // need to figure this one later
    ioSpecification.setAttribute("id", "IOSpecification" + generateID())


    let bpmn_inputSet = xmlDoc.createElement("bpmn:inputSet")
    // need to figure this one later

    let set_id = generateID()

    bpmn_inputSet.setAttribute("id", "InputSet_" + set_id)
    bpmn_inputSet.setAttribute("name", "Input Set " + set_id)


    let bpmn_outputSet = xmlDoc.createElement("bpmn:outputSet")
    // need to figure this one later
    bpmn_outputSet.setAttribute("id", "OutputSet_" + set_id)
    bpmn_outputSet.setAttribute("name", "Output Set" + set_id)


    // this is only for INPUTS
    // 1st loop
    for (let index = 0; index < mofos.children.length; index++) {
        // for each children of data input association we loop and check if the nodename is equal to bpmn sourceRef
        const dataAssociation = mofos.children[index].children
        // returns bpmn:dataInputAssociation or bpmn:dataOutputAssociation
        const upperLevel = mofos.children[index].nodeName

        // for each internal child loop through it
        // 2nd loop
        for (let i = 0; i < dataAssociation.length; i++) {
            const element = dataAssociation[i];
            // print the name of the task associated with this sourceRef
            //console.log(element.innerHTML);

            if (BlackListID.includes(element.innerHTML)) {

                handleNormalDataObjectReferences(xmlDoc, element, bpmn_outputSet, bpmn_inputSet, ioSpecification)

            }


            if ("bpmn:sourceRef" === element.nodeName && upperLevel === 'bpmn:dataInputAssociation'
                && !BlackListID.includes(element.innerHTML)) {

                // this is where we change it
                // source ref
                // target ref

                // generate random DataInput


                for (let i = 0; i < dataAssociations.length; i++) {
                    const pair = dataAssociations[i]

                    if (pair.source === element.innerHTML) {
                        element.parentNode.children[0].innerHTML = pair.source
                        element.parentNode.children[1].innerHTML = pair.target
                    }
                }

                //console.log(element.parentNode.children[1]);
                //outputs DataObjectReference which is the little Input

                // add bpmn:dataInput
                let dataInput = xmlDoc.createElement("bpmn:dataInput")
                dataInput.setAttribute("id", element.parentNode.children[1].innerHTML)
                dataInput.setAttribute("name", element.parentNode.children[1].innerHTML)

                // create data input ref
                let dataInputRefs = xmlDoc.createElement("bpmn:dataInputRefs")
                // need to figure this one later
                dataInputRefs.innerHTML = element.parentNode.children[1].innerHTML

                bpmn_inputSet.appendChild(dataInputRefs)
                ioSpecification.appendChild(dataInput)

                // CHANGE THE SHAPE AND ASSIGN THE BIG INPUT TO BE THE ONE RENDERED
                let shapes = xmlDoc.getElementsByTagName('bpmndi:BPMNShape')
                // loop through shapes
                for (let i = 0; i < shapes.length; i++) {
                    const shape = shapes[i];
                    //console.log(shape.id);
                    if (shape.id === element.parentNode.children[1].innerHTML + '_di') {
                        shape.setAttribute("bpmnElement", element.parentNode.children[0].innerHTML)
                        //console.log(shape.getAttribute("bpmnElement"));
                    }

                }
            }

            if ("bpmn:targetRef" === element.nodeName && upperLevel === 'bpmn:dataOutputAssociation'
                && !BlackListID.includes(element.innerHTML)) {

                let source = xmlDoc.createElement("bpmn:sourceRef")
                source.innerHTML = element.innerHTML

                // push the source to the children
                element.parentNode.appendChild(source)

                // the other one throws undefined, since there is no source
                //console.log(element.parentNode.children[1].innerHTML);

                for (let i = 0; i < dataAssociations.length; i++) {
                    const pair = dataAssociations[i]

                    console.log(pair.target);
                    console.log(element.innerHTML);

                    if (pair.target === element.innerHTML) {
                        element.parentNode.children[0].innerHTML = pair.target
                        element.parentNode.children[1].innerHTML = pair.source
                    }
                }

                //console.log(element.parentNode);

                // add bpmn:dataInput
                let dataOutput = xmlDoc.createElement("bpmn:dataOutput")
                dataOutput.setAttribute("id", element.parentNode.children[1].innerHTML)
                dataOutput.setAttribute("name", element.parentNode.children[1].innerHTML)

                // create data input ref
                let dataOutputRefs = xmlDoc.createElement("bpmn:dataOutputRefs")
                // need to figure this one later
                dataOutputRefs.innerHTML = element.parentNode.children[1].innerHTML


                bpmn_outputSet.appendChild(dataOutputRefs)
                ioSpecification.appendChild(dataOutput)

                // CHANGE THE SHAPE AND ASSIGN THE BIG INPUT TO BE THE ONE RENDERED
                let shapes = xmlDoc.getElementsByTagName('bpmndi:BPMNShape')
                // loop through shapes
                for (let i = 0; i < shapes.length; i++) {
                    const shape = shapes[i];
                    //console.log(shape.id);
                    if (shape.id === element.parentNode.children[1].innerHTML + '_di') {
                        shape.setAttribute("bpmnElement", element.parentNode.children[0].innerHTML)
                        //console.log(shape.getAttribute("bpmnElement"));
                    }

                }

            }

        }
    }

    if (bpmn_outputSet.children.length > 0) {
        ioSpecification.appendChild(bpmn_outputSet)
    }
    if (bpmn_inputSet.children.length > 0) {
        ioSpecification.appendChild(bpmn_inputSet)
    }

    mofos.appendChild(ioSpecification)
}

// we check if its a normal Data Object Reference, if its null, we black list it
function check_datatype(xmlDoc) {
    let dataInputs = xmlDoc.getElementsByTagName('bpmn:dataObjectReference')

    for (let i = 0; i < dataInputs.length; i++) {
        const element = dataInputs[i];
        //console.log(element.getAttribute("datatype"));
        if (element.getAttribute("datatype") === null) {
            //console.log(element.id);
            // check if it already exists
            if (!BlackListID.includes(element.id)) {
                BlackListID.push(element.id)
                BlackListDataObjRef.push(element.getAttribute("dataObjectRef"))
            }
        }
    }
}


function handleProcesses(xmlDoc) {
    // ENCAPSULATE DATA OBJ REFERENCE AND DATA OBJECTS INSIDE A BPMN IOSPECIFICATION FIELD

    let processes = xmlDoc.getElementsByTagName('bpmn:process')

    check_datatype(xmlDoc)

    // FOR EACH PROCESS
    for (let i = 0; i < processes.length; i++) {

        const tasks = processes[i].children;
        let process = processes[i]


        let encapsulativeIO = xmlDoc.createElement("bpmn:ioSpecification")

        let listOfElements = []
        let dataAssociations = []


        // check what is in the blacklist
        // console.log(BlackListID);


        // FOR EACH TASK
        for (let i = 0; i < tasks.length; i++) {
            const task = tasks[i];
            if (!BlackListID.includes(task.id) && !BlackListDataObjRef.includes(task.id)) {

                let inputs = task.getElementsByTagName('bpmn:dataInputAssociation')
                //console.log(inputs);
                // this should be the source;
                let source = null
                if (inputs.length > 0) {
                    for (let i = 0; i < inputs.length; i++) {
                        source = inputs[i].children[0].innerHTML

                        if (dataAssociations.length > 0) {
                            for (let i = 0; i < dataAssociations.length; i++) {
                                const dataAssociation = dataAssociations[i];
                                if (dataAssociation.source === source) {
                                    console.log('its already in there')
                                    break
                                }
                            }
                        }

                        let dataInput = xmlDoc.createElement("bpmn:dataInput")

                        let randomID = "DataInput_" + generateID()

                        dataInput.setAttribute("id", source)
                        dataInput.setAttribute("name", "Data Input " + generateID())


                        listOfElements.push(dataInput)

                        let pair = {
                            source: source,
                            target: randomID
                        }

                        dataAssociations.push(pair)

                    }

                }

                let outputs = task.getElementsByTagName('bpmn:dataOutputAssociation')

                let target = null
                if (outputs.length > 0) {
                    for (let i = 0; i < outputs.length; i++) {
                        target = outputs[i].children[0].innerHTML

                        if (dataAssociations.length > 0) {
                            for (let i = 0; i < dataAssociations.length; i++) {
                                const dataAssociation = dataAssociations[i];
                                if (dataAssociation.target === target) {
                                    console.log('its already in there')
                                    break
                                }
                            }
                        }

                        let dataOutput = xmlDoc.createElement("bpmn:dataOutput")

                        let randomID = "DataOutput_" + generateID()

                        dataOutput.setAttribute("id", target)
                        dataOutput.setAttribute("name", "Data Output " + generateID())


                        listOfElements.push(dataOutput)

                        let pair = {
                            source: randomID,
                            target: target
                        }

                        dataAssociations.push(pair)

                    }

                }

                //console.log(dataAssociations);

            }


            let regex = new RegExp('bpmn:.*Task', 'ig')

            if (regex.test(task.nodeName)) {
                xmlMagic(xmlDoc, task, dataAssociations)
            }


        }





        // APPEND EACH ELEMENT TO ITS AN BOX OF IOS THAT WE WILL LATER APPEND TO THE PROCESS
        for (let i = 0; i < listOfElements.length; i++) {
            const element = listOfElements[i];
            // add it directly to the process without attaching it to the encapsulative IO first
            //console.log(processes);

            // check if the element isnt contained in the blacklist first
            if (!BlackListID.includes(element.id)) {

                process.appendChild(element)

                encapsulativeIO.appendChild(element)

            }

        }

        if (listOfElements.length > 0) {
            //console.log(encapsulativeIO.children);
            processes[i].appendChild(encapsulativeIO)
        }

        let capsules = []
        // filter any repetitions in the encapsulativeIO
        for (let i = encapsulativeIO.children.length - 1; i >= 0; --i) {
            const element = encapsulativeIO.children[i];

            if(capsules.includes(element.id)) {
                element.remove()
            }

            if (!capsules.includes(element.id)) {
                capsules.push(element.id)
            }

        }

    }


}

/*

function findTaskElements(xmlDoc) {
    var tmp = xmlDoc.getElementsByTagName("*");
    var elArray = [];
    var regex = new RegExp('bpmn:.*Task', 'ig')
    for (var i = 0; i < tmp.length; i++) {
        if (regex.test(tmp[i].nodeName)) {
            elArray.push(tmp[i]);
        }
    }
    return elArray;
}

*/